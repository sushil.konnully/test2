﻿using UnityEngine;
using System.Collections;
using System;

public class ClockBehavior : MonoBehaviour
{
	public Transform hourHand;
	public Transform minuteHand;
	public Transform secondHand;

	private const float hoursToDegrees = 360 / 12;
	private const float minutesToDegrees = 360 / 60;
	private const float secondsToDegrees = 360 / 60;

//	void Start ()
//	{
//
//
//		Debug.Log (gameObject.name+" at start "+ Time.realtimeSinceStartup);
//	}
	
	// Update is called once per frame
	void Update ()
	{
		DateTime time = DateTime.Now;
		//Debug.Log (time.Hour);

		hourHand.localEulerAngles = new Vector3 (0, 0, -hoursToDegrees * time.Hour );
		minuteHand.localEulerAngles = new Vector3 (0, 0, -minutesToDegrees * time.Minute );
		secondHand.localEulerAngles = new Vector3 (0, 0, -secondsToDegrees * time.Second );
	}
}
